import json
import re
import sys

ALPHABET = [' ', 'a', 'b', 'c', 'č', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'r',
            's', 'š', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'ž']


def get_clean_text(text):
    text = text.replace('\t', '  ')
    clean_to_source = {}
    lower_text = text.lower().replace('\n', ' ')
    offset = 0
    clean_chars = []
    for i in range(len(text)):
        c = lower_text[i]
        if c not in ALPHABET:
            offset += 1
            continue
        clean_chars.append(c)
        clean_to_source[i - offset] = i
    return ''.join(clean_chars), clean_to_source, text


text = open(sys.argv[1], encoding='utf-8').read()
text = re.sub(' +', ' ', text)
aligned_words = json.loads(open(sys.argv[2], encoding='utf-8').read())

clean_text, clean_to_source, text = get_clean_text(text)
clean_text_positions = []

for c_word in clean_text.split(' '):
    if not aligned_words:
        break

    #if aligned_words[0]['text'] != c_word:
    #    continue
    if c_word == '':
        if len(clean_text_positions)>0:
            clean_text_positions.append(clean_text_positions[-1])  # space
        continue

    a_word = aligned_words.pop(0)
    print(c_word, a_word['text'], a_word['start'])
    k = (a_word['end'] - a_word['start']) / len(c_word)
    clean_text_positions += [a_word['start'] + i * k for i in range(len(c_word))]
    #clean_text_positions.append(clean_text_positions[-1])  # space

    clean_text_positions.append(a_word['end'])  # space


output = {
    "text": text,
    "text_clean": clean_text,
    "clean_to_source": clean_to_source,
    "clean_text_positions_s": clean_text_positions,
}


if len(sys.argv) == 4:
    with open(sys.argv[3], 'w', encoding='utf-8') as f:
        json.dump(output, f)
else:
    print(json.dumps(output))
